require 'github/markup'
filename = ARGV.first

puts "Analyzing #{filename}"
puts "Encoding according to 'file': #{IO.popen(%W(file #{filename})).read}"
rendered = GitHub::Markup.render(ARGV.first, File.read(ARGV.first))
puts "Encoding according to GitHub::Markup: #{rendered.encoding}"
