# Testing an encoding problem with GitHub::Markup and reStructuredText

```
bundle install
bundle exec ruby encoding_after_markup.rb example.rst
```

Issue being discussed in https://gitlab.com/gitlab-org/gitlab-ce/issues/296 .
