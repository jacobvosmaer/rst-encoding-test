========================
Aplicación web identICRT
========================
Esta distribución de software contiene la aplicación de gestión del servicio
identICRT del Instituto Cubano de Radio y Televisión, que permite la
administración de grupos, sus usuarios, y la información de servicios
externos.

Aunque el servicio en general, y la aplicación web en paticular, son más o
menos simples en cuanto a sus funciones, dependencias y fórmula de
despliegue, se ha hecho gran énfasis en la existencia de documentación de
muchos de sus aspectos. En el directorio ``doc`` de esta distribución
existen varios archivos relativos al diseño, la arquitectura, el desarrollo
y el despliegue en producción. Se recomienda introducirse, o comenzar la
lectura, a través de ``doc/README.rst``.
